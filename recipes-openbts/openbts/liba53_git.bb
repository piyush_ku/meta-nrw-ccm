DESCRIPTION = "A5/3 Call Encryption Library"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=33f1e9b996445fae3abdec0dc53f884f"

SRC_URI = "git://github.com/RangeNetworks/liba53.git;branch=master \
file://0001-Use-CXX-env-variable-for-easier-cross-compilation.patch \
"

SRCREV = "27354560dc7b554e03d40a520d41290e731193b6"
S = "${WORKDIR}/git"

do_install() {
	install -d ${D}/usr/lib
	install -d ${D}/usr/include
	oe_runmake install DESTDIR=${D}
}
