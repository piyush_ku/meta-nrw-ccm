DESCRIPTION = "GSM+GPRS Radio Access Network Node"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=a37ef17cc8984356a05bd6cbf322361c"

DEPENDS = "liba53 cppzmq ortp coredumper libusb1 sqlite3"

RDEPENDS_${PN} = "readline bash"

S = "${WORKDIR}/git"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "openbts_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

DEV_BRANCH  = "${@ 'nrw/litecell15-next' if d.getVar('NRW_CCM_DEVEL', False) == "next" else 'nrw/litecell15'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "gitsm://${NRW_NOA_MIRROR}/openbts.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/litecell15"
REL_SRCREV  = "57d9d444d200efbaec7a020a15e374196fda5617"
REL_SRCURI := "gitsm://${NRW_NOA_MIRROR}/openbts.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_SRCURI}'}"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

inherit autotools-brokensep pkgconfig

FILES_${PN}-dbg += "/OpenBTS/.debug"
FILES_${PN} += "/OpenBTS"
