DESCRIPTION = "SMQueue RFC-3428 Store and Forward Server"
LICENSE = "AGPL-3.0"
LIC_FILES_CHKSUM = "file://COPYING;md5=b5135ace5f72f03c4a4cf028321b9f0e"

DEPENDS = "cppzmq osip2 sqlite3"

S = "${WORKDIR}/git"

NRW_NOA_MIRROR ??= "git@gitlab.com/nrw_noa"

inherit gitver-pkg gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "smqueue_git.bb"
PR       := "r${REPOGITFN}"

PV   = "git${SRCPV}"
PKGV = "${PKGGITV}"

DEV_BRANCH  = "${@ 'nrw/litecell15-next' if d.getVar('NRW_CCM_DEVEL', False) == "next" else 'nrw/litecell15'}"
DEV_SRCREV  = "${AUTOREV}"
DEV_SRCURI := "gitsm://${NRW_NOA_MIRROR}/smqueue.git;protocol=ssh;branch=${DEV_BRANCH}"

REL_BRANCH  = "nrw/litecell15"
REL_SRCREV  = "804bf310852a50e028df7bee061e94e3cc53b505"
REL_SRCURI := "gitsm://${NRW_NOA_MIRROR}/smqueue.git;protocol=ssh;branch=${REL_BRANCH}"

BRANCH  = "${@ '${DEV_BRANCH}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_BRANCH}'}"
SRCREV  = "${@ '${DEV_SRCREV}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_SRCREV}'}"
SRC_URI = "${@ '${DEV_SRCURI}' if d.getVar('NRW_CCM_DEVEL', False) else '${REL_SRCURI}'}"

addtask showversion after do_compile before do_install
do_showversion() {
    bbplain "${PN}: ${PKGGITV} => ${BRANCH}:${PKGGITH}"
}

inherit autotools-brokensep pkgconfig

FILES_${PN}-dbg += "/OpenBTS/.debug"
FILES_${PN} += "/OpenBTS"
