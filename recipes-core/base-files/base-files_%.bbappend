BASEFILESISSUEINSTALL = "do_custom_baseissueinstall" 

inherit gitver-repo

REPODIR   = "${THISDIR}"
REPOFILE  = "base-files_%.bbappend"
PR       := "${PR}.${REPOGITFN}"
 
do_custom_baseissueinstall() { 
	do_install_basefilesissue 
 	install -m 644 ${WORKDIR}/issue*  ${D}${sysconfdir} 
	printf "LiteCell 1.5 CCM image (${DISTRO_VERSION}) " >> ${D}${sysconfdir}/issue 
 	printf "LiteCell 1.5 CCM image (${DISTRO_VERSION}) " >> ${D}${sysconfdir}/issue.net 
 	printf "\\\n \\\l\n" >> ${D}${sysconfdir}/issue 
 	echo "%h" >> ${D}${sysconfdir}/issue.net 
 	echo >> ${D}${sysconfdir}/issue 
 	echo >> ${D}${sysconfdir}/issue.net 
} 

