DESCRIPTION = "CCM default Package Groups"
LICENSE = "MIT"

inherit packagegroup

PACKAGES = "\
         packagegroup-ccm \
         "

RDEPENDS_packagegroup-ccm = "\
	python-ccm \
"

