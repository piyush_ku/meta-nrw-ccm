SUMMARY = "Litecell15 Linux image with CCM software installed"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58"

inherit gitver-repo

require ../../meta-nrw-bsc/recipes-core/images/litecell15-bsc-image.bb

REPODIR   = "${THISDIR}"
REPOFILE  = ""
PR       := "r${REPOGITFN}"
PV       := "${REPOGITT}-${REPOGITN}"

IMAGE_NAME = "${PN}-${PV}-${PR}-${DATETIME}"
CCM_LAYER_VERSION := "${PV}-${PR}"


install_lc15_manifest_append() {
    printf "           CCM : %s\n" "${CCM_LAYER_VERSION}" >> ${IMAGE_ROOTFS}/etc/litecell15.manifest
}

IMAGE_INSTALL += "packagegroup-ccm"


inherit core-image
