DESCRIPTION = "psutil is a cross-platform library for retrieving information onrunning processes and system utilization (CPU, memory, disks, network)in Python."
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

SRC_URI[md5sum] = "b34360627efb429cb18a4a3143b18c8c"
SRC_URI[sha256sum] = "d3290bd4a027fa0b3a2e2ee87728056fe49d4112640e2b8c2ea4dd94ba0cf057"

inherit pypi setuptools

RDEPENDS_${PN} = "python-subprocess"
