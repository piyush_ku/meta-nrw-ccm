SUMMARY = "meld3 is an HTML/XML templating engine."
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://LICENSE.txt;md5=9e7581cef5645475fcefebdc15ed7abf"

SRC_URI[md5sum] = "3ccc78cd79cffd63a751ad7684c02c91"
SRC_URI[sha256sum] = "f7b754a0fde7a4429b2ebe49409db240b5699385a572501bb0d5627d299f9558"

inherit pypi setuptools
