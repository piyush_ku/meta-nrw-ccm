SUMMARY = "makes web apps"
LICENSE = "PD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/PD;md5=b3597d12946881e13cb3b548d1173851"

SRC_URI[md5sum] = "93375e3f03e74d6bf5c5096a4962a8db"
SRC_URI[sha256sum] = "748c7e99ad9e36f62ea19f7965eb7dd7860b530e8f563ed60ce3e53e7409a550"

PYPI_PACKAGE = "web.py"

inherit pypi setuptools
