SUMMARY = "Simple persistent unique IDs"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

SRC_URI[md5sum] = "3c8ff14fde4b19bda3b98cf38e023d79"
SRC_URI[sha256sum] = "03776b8e97b8b3a0f5db7ee2be4070db380ed1561d51bb1046544c5149c33e61"

inherit pypi setuptools
