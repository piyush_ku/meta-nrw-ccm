SUMMARY = "collection of low-level Python packages and modules used by Logilab projects."
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRC_URI[md5sum] = "a2cf70875cc1ca9edcd7b17f27f1d88f"
SRC_URI[sha256sum] = "a66146e54e632b008cc2c5af05d546e293c4d6203be702c9a97600c5af1605fa"

inherit pypi setuptools

RDEPENDS_${PN} += "python-six python-setuptools"
