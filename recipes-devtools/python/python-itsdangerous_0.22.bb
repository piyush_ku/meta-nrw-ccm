DESCRIPTION = "Various helpers to pass trusted data to untrusted environments"
HOMEPAGE = "https://github.com/pallets/itsdangerous"
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

SRC_URI = "https://github.com/pallets/itsdangerous/archive/0.22.zip"
SRC_URI[md5sum] = "0e2df9ab4eae0e375a1c9b5a09e98a65"
SRC_URI[sha256sum] = "a016b398e7621e8312ffb248a25a0b3fd36b04897b70800995edbd35f59e3180"
S = "${WORKDIR}/itsdangerous-${PV}"

inherit setuptools

do_configure_preprend() {
	rm ${S}/Makefile
}

CLEANBROKEN = "1"
