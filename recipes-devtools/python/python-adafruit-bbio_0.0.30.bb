DESCRIPTION = "A module to control BeagleBone IO channels"
LICENSE = "MIT"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/MIT;md5=0835ade698e0bcf8506ecda2f7b4f302"

SRC_URI[md5sum] = "5f99f0caf52047ab3c9f8236cddaf8f1"
SRC_URI[sha256sum] = "122364531029956d0f787e7d4f0fe4c14e701babd184f69869c20a24d3083149"

PYPI_PACKAGE = "Adafruit_BBIO"

inherit pypi setuptools
