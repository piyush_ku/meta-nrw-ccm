SUMMARY = "SMS PDU encoding and decoding, including GSM-0338 character set"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

SRC_URI[md5sum] = "d350d9923c9a943c8e8af6825a41f529"
SRC_URI[sha256sum] = "22467948c5c429c76449855d49616f751c7ee5a71fb017f3b9e0537f820791f9"

inherit pypi setuptools
