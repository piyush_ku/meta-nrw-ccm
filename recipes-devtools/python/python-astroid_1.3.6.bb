SUMMARY = "An abstract syntax tree for Python with inference support."
LICENSE = "LGPLv2"
LIC_FILES_CHKSUM = "file://COPYING;md5=751419260aa954499f7abaabaa882bbe"

SRC_URI[md5sum] = "0d387f5b2e878f424b95af3bfe44e106"
SRC_URI[sha256sum] = "1241ef961448c57b4616beb8dcc959724641dca1e22914663f79d67fec26f854"

inherit pypi setuptools

RDEPENDS_${PN} += "python-six python-logilab-common"
