DESCRIPTION = "Random assortment of WSGI servers"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

PV = "1.0.3.dev-20110405"

SRC_URI[md5sum] = "a005b072d144fc0e44b0fa4c5a9ba029"
SRC_URI[sha256sum] = "6649cf41854ea8782c795cdde64fdf79a90db821533d3652f91d21b0a7f39c79"

inherit pypi setuptools
