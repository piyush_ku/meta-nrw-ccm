SUMMARY = "WSGI (PEP 333) Reference Library."
LICENSE = "Python-2.0"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/Python-2.0;md5=a5c8025e305fb49e6d405769358851f6"

SRC_URI[md5sum] = "29b146e6ebd0f9fb119fe321f7bcf6cb"
SRC_URI[sha256sum] = "c7e610c800957046c04c8014aab8cce8f0b9f0495c8cd349e57c1f7cabf40e79"

PYPI_PACKAGE_EXT = "zip"

inherit pypi setuptools
