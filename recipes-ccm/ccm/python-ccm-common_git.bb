DESCRIPTION = "Currency utilities to convert between standard currency representations and internal database integer representation."
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

require ccm.inc

SRC_URI = "git://github.com/facebookincubator/CommunityCellularManager.git;branch=master"
SRCREV = "${CCMREV}"
S = "${WORKDIR}/git/common"

RDEPENDS_${PN} = "python-six python-wheel"

inherit setuptools
