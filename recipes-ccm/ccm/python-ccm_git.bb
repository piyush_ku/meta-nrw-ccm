SUMMARY = "Community Cellular Manager (CCM) client software for the BTS."
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://${COMMON_LICENSE_DIR}/BSD;md5=3775480a712fc46a69647678acb234cb"

require ccm.inc

SRC_URI = "git://github.com/facebookincubator/CommunityCellularManager.git;branch=master \
file://etage-bundle.crt \
"

SRCREV = "${CCMREV}"
S = "${WORKDIR}/git/client"

RDEPENDS_${PN} = " \
python-argparse \
python-astroid \
python-babel \
python-coverage \
python-ccm-common \
python-docopt \
python-eno \
python-envoy \
python-flup \
python-humanize \
python-itsdangerous \
python-logilab-common \ 
python-meld3 \
python-mock \
python-netifaces \
python-nose \
python-openbts \
python-paste \
python-phonenumbers \
python-psutil \
python-psycopg2 \
python-pyyaml \
python-pyzmq \
python-requests \
python-six \
python-smsutilities \
python-snowflake \
python-webpy \
python-wsgiref \
python-twisted \
python-dateutil \
python-pytz \
python-enum34 \
python-wsgiref \
freeswitch \
postgresql \
openvpn \
"

inherit setuptools

do_install_prepend() {
	install -m 0644 ${WORKDIR}/etage-bundle.crt ${S}/conf/registration/etage-bundle.crt
}

FILES_${PN} += "/usr/share/freeswitch"
