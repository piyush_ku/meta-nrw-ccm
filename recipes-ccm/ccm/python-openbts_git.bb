SUMMARY = "Python client for the OpenBTS NodeManager."
SECTION = "devel/python"
LICENSE = "BSD"
LIC_FILES_CHKSUM = "file://openbts/__init__.py;beginline=8;endline=13;md5=fb3b6e9fe77307b9c1ffa9bb207d9e7b"

require ccm.inc

SRC_URI = "git://github.com/facebookincubator/CommunityCellularManager.git;branch=master"
SRCREV = "${CCMREV}"
S = "${WORKDIR}/git/openbts-python"

RDEPENDS_${PN} = " \
python-argparse \
python-coverage \
python-enum34 \
python-envoy \
python-mock \
python-pytz \
python-wsgiref \
openbts \
smqueue \
sipauthserve \
"

inherit setuptools
